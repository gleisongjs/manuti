# -*- coding: utf-8 -*-
from __future__ import unicode_literals


from annoying.decorators import render_to

# Create your views here.

from django.urls import reverse_lazy
from django.views.generic import TemplateView, ListView, UpdateView, CreateView, DeleteView, DetailView, RedirectView
from manuti.models import RegistroDeManut, Equipamento, Aeronave, UsuarioMarcafut, Emergencia, Suprimento, Ferramenta, CodigoPane
from forms import InsereRegistroForm, InsereEquipamentoForm, InsereUsuarioForm, InsereEmergenciaForm, InsereSuprimentoForm, \
InsereFerramentaForm, InserePaneForm
from bootstrap_modal_forms.mixins import PassRequestMixin
from django.contrib.messages.views import SuccessMessageMixin
from django.shortcuts import render, HttpResponse
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.contrib.auth import authenticate, login, get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.contrib.auth.models import Group
from django.contrib.auth.decorators import permission_required
from django.contrib.auth.mixins import PermissionRequiredMixin
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.db.models import Q
from django.shortcuts import get_object_or_404



#PÁGINA PRINCIPAL
# ----------------------------------------------

class IndexManutiTemplateView(TemplateView):
    
       
    activeferramenta = ''
    activeemergencia = ''
    activesiloms = ''
    activeatividade = ''
    activeusuario = ''
    activeindex = 'active'
    activepane = ''

    template_name = "manuti/index.html"

    def get_context_data(self, **kwargs):
        
        context = super(IndexManutiTemplateView, self).get_context_data(**kwargs)
        context['registros'] = len(RegistroDeManut.objetos.all())
        context['aeronaves'] = len(Aeronave.objetos.all())
        context['siloms'] = len(RegistroDeManut.objetos.filter(resolvesiloms=True))
        context['sil'] = len(RegistroDeManut.objetos.all())
        context['emergencias'] = len(Emergencia.objetos.filter(resolveemergencia=True))
        context['emer'] = len(Emergencia.objetos.all())
        context['equipamentos'] = len(Equipamento.objetos.all())
        return context

@method_decorator(login_required, name='dispatch')        
class AtividadeManutiTemplateView(TemplateView, LoginRequiredMixin):
    activeferramenta = ''
    activeemergencia = ''
    activesiloms = ''
    activeatividade = 'active'
    activeusuario = ''
    activeindex = ''
    activepane = ''

    template_name = "manuti/atividademanuti.html"
    context_object_name = "atividademanuti"

# LISTA DE Registros
# ----------------------------------------------

@method_decorator(login_required, name='dispatch')
class RegistroListView(LoginRequiredMixin, ListView):
    
    activeferramenta = ''
    activecombs = ''
    activesiloms = ''
    activeatividade = 'active'
    activeusuario = ''
    activeindex = ''
    activepane = ''

    template_name = "manuti/lista.html"
    #model = RegistroDeManut
    context_object_name = "registros"
    #queryset = RegistroDeManut.objetos.order_by('-data')
    def get_queryset(self):
        
        #Funcionando com filtro por usuário
        #registros = RegistroDeManut.objetos.filter(usuario=self.request.user).order_by('-data')
        #teste = self.request.user.groups.all()
        #print teste, 'xxxxxxxxxxxx'

        busca = self.request.GET.get('search')

        if busca:
            registros = RegistroDeManut.objetos.filter(Q(data__icontains=busca) | Q(titulo__icontains=busca) | Q(ocorrencia__icontains=busca) | Q(solucao__icontains=busca), \
                usuario=self.request.user).order_by('-data')
        else:
            registros = RegistroDeManut.objetos.filter(usuario=self.request.user).order_by('-data')

        
        return registros
    

# CADASTRAMENTO DE Registro
# ----------------------------------------------

@method_decorator(login_required, name='dispatch')
class RegistroCreateView(LoginRequiredMixin,CreateView):
    
    activeferramenta = ''
    activecombs = ''
    activesiloms = ''
    activeatividade = 'active'
    activeusuario = ''
    activeindex = ''
    activepane = ''

    template_name = "manuti/cria.html"
    model = RegistroDeManut
    form_class = InsereRegistroForm
    success_url = reverse_lazy("manuti:atividade_manuti")
    def form_valid(self, form):
        form.instance.usuario = self.request.user
        return super(RegistroCreateView, self).form_valid(form)
    

      


# ATUALIZAÇÃO DE Registros
# ----------------------------------------------
@method_decorator(login_required, name='dispatch')
class RegistroUpdateView(UpdateView, LoginRequiredMixin):
    
    activeferramenta = ''
    activeemergencia = ''
    activesiloms = ''
    activeatividade = 'active'
    activeusuario = ''
    activeindex = ''
    activepane = ''

    template_name = "manuti/atualiza.html"
    model = RegistroDeManut
    form_class = InsereRegistroForm
    context_object_name = 'registro'
    success_url = reverse_lazy("manuti:lista_registros")
    def form_valid(self, form):
        form.instance.usuario = self.request.user
        return super(RegistroUpdateView , self).form_valid(form)


# EXCLUSÃO DE Registros
# ----------------------------------------------
@method_decorator(login_required, name='dispatch')
class RegistroDeleteView(DeleteView, LoginRequiredMixin):
    
    activeferramenta = ''
    activeemergencia = ''
    activesiloms = ''
    activeatividade = 'active'
    activeusuario = ''
    activeindex = ''
    activepane = ''

    template_name = "manuti/exclui.html"
    model = RegistroDeManut
    fields = '__all__'
    context_object_name = 'registro'
    success_url = reverse_lazy("manuti:lista_registros")

#------------------------------------------------------------------------------------------------------

# LISTA DE Equipamentos
# ----------------------------------------------
@method_decorator(login_required, name='dispatch')
class EquipamentoListView(ListView, LoginRequiredMixin):
    
    activeferramenta = ''
    activeemergencia = ''
    activesiloms = ''
    activeatividade = 'active'
    activeusuario = ''
    activeindex = ''
    activepane = ''

    template_name = "manuti/listaEquipamento.html"
    #model = Equipamento
    context_object_name = "equipamentos"
    #queryset = Equipamento.objetos.order_by('-id')
    def get_queryset(self):

        busca = self.request.GET.get('search')

        if busca:
            equipamentos = Equipamento.objetos.filter(Q(nome__icontains=busca) | Q(serialnumber__icontains=busca) | Q(partnumber__icontains=busca), \
                usuario=self.request.user).order_by('-id')
        else:
            equipamentos = Equipamento.objetos.filter(usuario=self.request.user).order_by('-id')

        return equipamentos
    

# CADASTRAMENTO DE Equipamentos
# ----------------------------------------------
@method_decorator(login_required, name='dispatch')
class EquipamentoCreateView(CreateView, LoginRequiredMixin):
    
    activeferramenta = ''
    activeemergencia = ''
    activesiloms = ''
    activeatividade = 'active'
    activeusuario = ''
    activeindex = ''
    activepane = ''

    template_name = "manuti/criaEquipamento.html"
    model = Equipamento
    form_class = InsereEquipamentoForm
    success_url = reverse_lazy("manuti:lista_equipamentos")
    def form_valid(self, form):
        form.instance.usuario = self.request.user
        return super(EquipamentoCreateView, self).form_valid(form)


# ATUALIZAÇÃO DE Equipamentos
# ----------------------------------------------
@method_decorator(login_required, name='dispatch')
class EquipamentoUpdateView(UpdateView, LoginRequiredMixin):
    
    activeferramenta = ''
    activeemergencia = ''
    activesiloms = ''
    activeatividade = 'active'
    activeusuario = ''
    activeindex = ''
    activepane = ''

    template_name = "manuti/atualizaEquipamento.html"
    model = Equipamento
    form_class = InsereEquipamentoForm
    context_object_name = 'equipamento'
    success_url = reverse_lazy("manuti:atividade_manuti")
    def form_valid(self, form):
        form.instance.usuario = self.request.user
        return super(EquipamentoUpdateView, self).form_valid(form)


# EXCLUSÃO DE Equipamentos
# ----------------------------------------------
@method_decorator(login_required, name='dispatch')
class EquipamentoDeleteView(DeleteView, LoginRequiredMixin):
    
    activeferramenta = ''
    activeemergencia = ''
    activesiloms = ''
    activeatividade = 'active'
    activeusuario = ''
    activeindex = ''
    activepane = ''

    template_name = "manuti/excluiEquipamento.html"
    model = Equipamento
    fields = '__all__'
    context_object_name = 'equipamento'
    success_url = reverse_lazy("manuti:lista_equipamentos")

#------------------------------------------------------------------------------------------------------

# LISTA DE Aeronaves
# ----------------------------------------------

class AeronaveListView(ListView, LoginRequiredMixin):
    
    activeferramenta = ''
    activeemergencia = ''
    activesiloms = ''
    activeatividade = 'active'
    activeusuario = ''
    activeindex = ''
    activepane = ''

    template_name = "manuti/listaAnv.html"
    #model = Aeronave
    context_object_name = "aeronaves"
    queryset = Aeronave.objetos.order_by('matricula')

@method_decorator(login_required, name='dispatch')
class AeronaveRegistroListView(ListView, LoginRequiredMixin):
    
    activeferramenta = ''
    activeemergencia = ''
    activesiloms = ''
    activeatividade = 'active'
    activeusuario = ''
    activeindex = ''
    activepane = ''

    template_name = "manuti/visualizaanv.html"
    #model = Aeronave
    context_object_name = "visualizaanvs"
    #queryset = Aeronave.objetos.order_by('aeronava_id')
    #queryset = Aeronave.objetos.filter(unidades_id='1').order_by('nome')
    queryset = Aeronave.objetos.order_by('matricula')

    

    
# CADASTRO DE Usuários
# ----------------------------------------------

class UsuarioCreateView(CreateView):
    
    activeferramenta = ''
    activeemergencia = ''
    activesiloms = ''
    activeatividade = ''
    activeusuario = 'active'
    activeindex = ''
    activepane = ''

    template_name = "manuti/criaUsuario.html"
    model = UsuarioMarcafut
    form_class = InsereUsuarioForm
    success_url = reverse_lazy("manuti:index")


# LISTA DE Relacionamentos por Aeronaves
# ----------------------------------------------
@method_decorator(login_required, name='dispatch')
class EquipamentoAeronaveListView(ListView, LoginRequiredMixin):
    
    activeferramenta = ''
    activeemergencia = ''
    activesiloms = ''
    activeatividade = 'active'
    activeusuario = ''
    activeindex = ''
    activepane = ''

    template_name = "manuti/listaequipamentos.html"
    #model = Equipamento
    context_object_name = "equipamentoanvs"
    
    #Realiza o filtro do combs
    #queryset = Equipamento.objetos.filter(combs=True)
     
    def get_queryset(self):
        fk = self.kwargs['fk']

        busca = self.request.GET.get('search')

        if busca:
            registroEquipamento = Equipamento.objetos.filter(Q(nome__icontains=busca) | Q(serialnumber__icontains=busca) | Q(partnumber__icontains=busca), \
                aeronave_id=fk, usuario=self.request.user).order_by('nome')
        else:
            registroEquipamento = Equipamento.objetos.filter(aeronave_id=fk, usuario=self.request.user).order_by('nome')

        
        return registroEquipamento
    
@method_decorator(login_required, name='dispatch')
class RegistroAeronaveListView(ListView, LoginRequiredMixin):
    
    activeferramenta = ''
    activeemergencia = ''
    activesiloms = ''
    activeatividade = 'active'
    activeusuario = ''
    activeindex = ''
    activepane = ''

    template_name = "manuti/listaregistros.html"
    context_object_name = "registroanvs"
    
    #Realiza o filtro do combs
    #queryset = Equipamento.objetos.filter(combs=True)
    
    def get_queryset(self):
        fk = self.kwargs['fk']

        busca = self.request.GET.get('search')

        if busca:
            registroAnv = RegistroDeManut.objetos.filter(Q(titulo__icontains=busca) | Q(ocorrencia__icontains=busca) | Q(solucao__icontains=busca), \
                 anv=fk,usuario=self.request.user).order_by('-data')
        else:
            registroAnv = RegistroDeManut.objetos.filter(anv=fk,usuario=self.request.user ).order_by('-data')


        return registroAnv

@method_decorator(login_required, name='dispatch')
class EquipamentoAeronaveDeleteView(DeleteView, LoginRequiredMixin):
    
    activeferramenta = ''
    activeemergencia = ''
    activesiloms = ''
    activeatividade = 'active'
    activeusuario = ''
    activeindex = ''
    activepane = ''

    template_name = "manuti/excluiEquipamentoanv.html"
    model = Equipamento
    fields = '__all__'
    context_object_name = "equipamentoanvsdelete"    
    success_url = reverse_lazy("manuti:lista_aeronaves")

    

#Siloms e Combs
# ----------------------------------------------  
#Listando Combs
@method_decorator(login_required, name='dispatch')
class CombsListView(ListView):
    
    activeferramenta = ''
    activeemergencia = 'active'
    activesiloms = ''
    activeatividade = ''
    activeusuario = ''
    activeindex = ''
    activepane = ''

    template_name = "manuti/combs.html"
    #model = Equipamento
    context_object_name = "combs"

    

    def get_queryset(self):
        return Equipamento.objetos.filter(combs=True).order_by('-id')

       
#Listando Siloms
@method_decorator(login_required, name='dispatch')
class SilomsListView(ListView, LoginRequiredMixin):
    
    activeferramenta = ''
    activeemergencia = ''
    activesiloms = 'active'
    activeatividade = ''
    activeusuario = ''
    activeindex = ''
    activepane = ''

    template_name = "manuti/siloms.html"
    context_object_name = "siloms"
    

    def get_queryset(self):
        #equipamento = Equipamento.objetos.all().order_by('-id')
        registros = RegistroDeManut.objetos.filter(usuario=self.request.user).order_by('-data')
        return registros
    

#Resolvendo Combs
@method_decorator(login_required, name='dispatch')
class CombsResolvidoUpdateView(RedirectView, LoginRequiredMixin):
        
    permanent = False
    query_string = True
    pattern_name = 'manuti:combs'
    

    def get_redirect_url(self, *args, **kwargs):
        #print 'começando'
        equipamento = get_object_or_404(Equipamento, pk=kwargs['pk'])

        if (equipamento.resolvecombs == True):
            equipamento.resolvecombs = False
            equipamento.save()
            print 'False salvo'
        else:
            equipamento.resolvecombs = True
            equipamento.save()
            print 'True salvo'

        return reverse('manuti:combs')
        

#Resolvendo Siloms
@method_decorator(login_required, name='dispatch')
class SilomsResolvidoUpdateView(RedirectView, LoginRequiredMixin):
        
    permanent = False
    query_string = True
    pattern_name = 'manuti:siloms'
    

    def get_redirect_url(self, *args, **kwargs):
        #print 'começando'
        registro = get_object_or_404(RegistroDeManut, pk=kwargs['pk'])

        if (registro.resolvesiloms == True):
            registro.resolvesiloms = False
            registro.save()
            print 'False salvo'
        else:
            registro.resolvesiloms = True
            registro.save()
            print 'True salvo'

        return reverse('manuti:siloms')



# CADASTRAMENTO DE Emergência
# ----------------------------------------------
@method_decorator(login_required, name='dispatch')
class EmergenciaCreateView(CreateView, LoginRequiredMixin):
    
    activeferramenta = ''
    activeemergencia = 'active'
    activesiloms = ''
    activeatividade = ''
    activeusuario = ''
    activeindex = ''
    activepane = ''

    template_name = "manuti/criaemergencia.html"
    model = Emergencia
    form_class = InsereEmergenciaForm
    success_url = reverse_lazy("manuti:emergencias")
    def form_valid(self, form):
        form.instance.usuario = self.request.user
        return super(EmergenciaCreateView, self).form_valid(form)

# LISTA DE Emergências
# ----------------------------------------------
@method_decorator(login_required, name='dispatch')
class EmergenciaListView(ListView, LoginRequiredMixin):
    
    activeferramenta = ''
    activeemergencia = 'active'
    activesiloms = ''
    activeatividade = ''
    activeusuario = ''
    activeindex = ''
    activepane = ''

    template_name = "manuti/listaemergencia.html"
    #model = RegistroDeManut
    context_object_name = "emergencias"
    def get_queryset(self):
        #equipamento = Equipamento.objetos.all().order_by('-id')
        emergencias = Emergencia.objetos.filter(usuario=self.request.user).order_by('-data')
        return emergencias

        

#Resolvendo Emergencias
@method_decorator(login_required, name='dispatch')
class EmergenciaResolvidoUpdateView(RedirectView, LoginRequiredMixin):
        
    permanent = False
    query_string = True
    pattern_name = 'manuti:emergencias'
    

    def get_redirect_url(self, *args, **kwargs):
        #print 'começando'
        emergencia = get_object_or_404(Emergencia, pk=kwargs['pk'])

        if (emergencia.resolveemergencia == True):
            emergencia.resolveemergencia = False
            emergencia.save()
            print 'False salvo'
        else:
            emergencia.resolveemergencia = True
            emergencia.save()
            print 'True salvo'

        return reverse('manuti:emergencias')

@method_decorator(login_required, name='dispatch')
class EmergenciaUpdateView(UpdateView, LoginRequiredMixin):
    
    
    template_name = "manuti/alteraremergencia.html"
    model = Emergencia
    form_class = InsereEmergenciaForm
    context_object_name = 'emergencia'
    success_url = reverse_lazy("manuti:emergencias")
    def form_valid(self, form):
        form.instance.usuario = self.request.user
        return super(EmergenciaUpdateView, self).form_valid(form)

# LISTA DE Suprimento
# ----------------------------------------------
@method_decorator(login_required, name='dispatch')
class SuprimentoListView(ListView, LoginRequiredMixin):
    
    
    template_name = "manuti/listaSuprimento.html"
    #model = Equipamento
    context_object_name = "suprimentos"
    def get_queryset(self):
        #equipamento = Equipamento.objetos.all().order_by('-id')
        suprimentos = Suprimento.objetos.filter(usuario=self.request.user).order_by('-data')
        return suprimentos


# CADASTRAMENTO DE Suprimento
# ----------------------------------------------
@method_decorator(login_required, name='dispatch')
class SuprimentoCreateView(CreateView, LoginRequiredMixin):
    
    
    template_name = "manuti/criaSuprimento.html"
    model = Suprimento
    form_class = InsereSuprimentoForm
    success_url = reverse_lazy("manuti:atividade_manuti")
    def form_valid(self, form):
        form.instance.usuario = self.request.user
        return super(SuprimentoCreateView, self).form_valid(form)


# ATUALIZAÇÃO DE Suprimento
# ----------------------------------------------
@method_decorator(login_required, name='dispatch')
class SuprimentoUpdateView(UpdateView, LoginRequiredMixin):
    
    
    template_name = "manuti/atualizaSuprimento.html"
    model = Suprimento
    form_class = InsereSuprimentoForm
    context_object_name = 'suprimento'
    success_url = reverse_lazy("manuti:lista_suprimento")
    def form_valid(self, form):
        form.instance.usuario = self.request.user
        return super(SuprimentoUpdateView, self).form_valid(form)


# EXCLUSÃO DE Suprimento
# ----------------------------------------------
@method_decorator(login_required, name='dispatch')
class SuprimentoDeleteView(DeleteView, LoginRequiredMixin):
    

    template_name = "manuti/excluiSuprimento.html"
    model = Suprimento
    fields = '__all__'
    context_object_name = 'suprimento'
    success_url = reverse_lazy("manuti:lista_suprimento")

# LISTA DE Ferramenta
# ----------------------------------------------

@method_decorator(login_required, name='dispatch')
class FerramentaListView(ListView, LoginRequiredMixin):
    
    activeferramenta = 'active'
    activeemergencia = ''
    activesiloms = ''
    activeatividade = ''
    activeusuario = ''
    activeindex = ''
    activepane = ''

    template_name = "manuti/listaFerramenta.html"
    #model = Equipamento
    context_object_name = "ferramentas"
    
    def get_queryset(self):
        #equipamento = Equipamento.objetos.all().order_by('-id')
        busca = self.request.GET.get('search')

        if busca:
            ferramentas = Ferramenta.objetos.filter(Q(nome__icontains=busca) | Q(descricao__icontains=busca), usuario=self.request.user).order_by('nome')
        else:
            ferramentas = Ferramenta.objetos.filter(usuario=self.request.user).order_by('nome')

        
        return ferramentas


# CADASTRAMENTO DE Ferramenta
# ----------------------------------------------
@method_decorator(login_required, name='dispatch')
class FerramentaCreateView(LoginRequiredMixin, CreateView):
    
    activeferramenta = 'active'
    activeemergencia = ''
    activesiloms = ''
    activeatividade = ''
    activeusuario = ''
    activeindex = ''
    activepane = ''

    template_name = "manuti/criaFerramenta.html"
    model = Ferramenta
    form_class = InsereFerramentaForm
    success_url = reverse_lazy("manuti:ferramentas")
    def form_valid(self, form):
        form.instance.usuario = self.request.user
        return super(FerramentaCreateView, self).form_valid(form)


# ATUALIZAÇÃO DE Ferramenta
# ----------------------------------------------
@method_decorator(login_required, name='dispatch')
class FerramentaUpdateView(UpdateView, LoginRequiredMixin):
    
    activeferramenta = 'active'
    activeemergencia = ''
    activesiloms = ''
    activeatividade = ''
    activeusuario = ''
    activeindex = ''
    activepane = ''

    template_name = "manuti/atualizaFerramenta.html"
    model = Ferramenta
    form_class = InsereFerramentaForm
    context_object_name = 'ferramenta'
    success_url = reverse_lazy("manuti:ferramentas")
    def form_valid(self, form):
        form.instance.usuario = self.request.user
        return super(FerramentaUpdateView, self).form_valid(form)

    
# EXCLUSÃO DE Ferramenta
# ----------------------------------------------
@method_decorator(login_required, name='dispatch')
class FerramentaDeleteView(DeleteView, LoginRequiredMixin):
    
    activeferramenta = 'active'
    activeemergencia = ''
    activesiloms = ''
    activeatividade = ''
    activeusuario = ''
    activeindex = ''
    activepane = ''

    template_name = "manuti/excluiFerramenta.html"
    model = Ferramenta
    fields = '__all__'
    context_object_name = 'ferramenta'
    success_url = reverse_lazy("manuti:ferramentas")


# LISTA DE Panes
# ----------------------------------------------

@method_decorator(login_required, name='dispatch')
class PanesTemplateView(ListView, LoginRequiredMixin):
    
    activeferramenta = ''
    activeemergencia = ''
    activesiloms = ''
    activeatividade = ''
    activeusuario = ''
    activeindex = ''
    activepane = 'active'
    
    template_name = "manuti/codPane.html"
    context_object_name = "panes"
    
    def get_queryset(self):
        
        
        busca = self.request.GET.get('search')

        if busca:
            panes = CodigoPane.objetos.filter(Q(codigo__icontains=busca) | Q(descricao__icontains=busca), \
                usuario=self.request.user).order_by('codigo')
        else:
            panes = CodigoPane.objetos.filter(id=00)
       
        return panes
    
    
     