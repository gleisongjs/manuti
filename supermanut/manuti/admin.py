# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from models import Unidade, Setor, Aeronave, Equipamento, RegistroDeManut, CodigoPane

# Register your models here.
admin.site.register(Unidade)
admin.site.register(Setor)
admin.site.register(Aeronave)
admin.site.register(Equipamento)
admin.site.register(RegistroDeManut)
admin.site.register(CodigoPane)