# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User


# Create your models here.

#Unidade
class Unidade(models.Model):
    nome = models.CharField(
        max_length=20,
        null=False,
        blank=False
    )
    
    local = models.CharField(
        max_length=30,
        null=False, 
        blank = False
    )

    def __unicode__(self):
        return self.nome

    objetos = models.Manager()

#Setor
class Setor(models.Model):
    
    nome = models.CharField(
        max_length=20,
        null=False,
        blank=False
    )
    ramal = models.IntegerField(
        default=0,
        null=False,
        blank=False
    )
    unidade = models.ForeignKey(Unidade)

    def __unicode__(self):
        return self.nome

    objetos = models.Manager()

#Aeronave
class Aeronave(models.Model):

        
    nome = models.CharField(
        max_length=20,
        null=False,
        blank=False
    )
    matricula = models.IntegerField(
        default=0,
        null=False,
        blank=False
    )
    
    #classe para ordenar as aeronaves!!!
    class Meta:
        ordering = ('matricula',)

    unidades = models.ForeignKey(Unidade)

    def __unicode__(self):
        return self.nome

    objetos = models.Manager()

#Equipamento
class Equipamento(models.Model):

    usuario = models.ForeignKey(User, on_delete=models.CASCADE, default=4)
    
    nome = models.CharField(
        max_length=20,
        null=False,
        blank=False
    )
    partnumber = models.CharField(
        max_length=20,
        null=False,
        blank=False
    )
    serialnumber = models.CharField(
        max_length=20,
        null=False,
        blank=False
    )
    combs = models.BooleanField(
        default = False,
    )

    resolvecombs = models.BooleanField(
        default = False,
    )

    
    aeronave = models.ForeignKey(Aeronave)

    def __unicode__(self):
        return self.nome
    
    objetos = models.Manager()


#RegistroDeManut
class RegistroDeManut(models.Model):
    
    id = models.AutoField(primary_key=True)
    
    data = models.DateField(null=True, blank=True)

    usuario = models.ForeignKey(User, on_delete=models.CASCADE, default=4)

    tempo = models.FloatField(
        default=0,
        null=False,
        blank=False
    )
    
    anv = models.ForeignKey(Aeronave)

    titulo = models.CharField(
        max_length=255,
        null=False,
        blank=False
        #default='test',
    )
    
    ocorrencia = models.TextField(
        max_length=1000,
        null=False,
        blank=False
    )
    solucao = models.TextField(
        max_length=1000,
        null=False,
        blank=False
    )

    resolvesiloms = models.BooleanField(
        default = False,
    )

    #registros = models.ForeignKey(Aeronave)
    def __unicode__(self):
        return self.titulo

    objetos = models.Manager()


#Emergências
class Emergencia(models.Model):
        
    id = models.AutoField(primary_key=True)
    
    data = models.DateField(null=True, blank=True)

    anv = models.ForeignKey(Aeronave)

    usuario = models.ForeignKey(User, on_delete=models.CASCADE, default=4)

    emergencia = models.CharField(
        max_length=20,
        null=False,
        blank=False
        
    )

    tipo = models.CharField(
        max_length=10,
        null=False,
        blank=False
        
    )
    
    ordemdeservico = models.CharField(
        max_length=20,
        null=False,
        blank=False
    )
    equipamento = models.CharField(
        max_length=100,
        null=False,
        blank=False
    )

    resolveemergencia = models.BooleanField(
        default = False,
    )

    #emergencias = models.ForeignKey(Aeronave)
    def __unicode__(self):
        return self.emergencia

    objetos = models.Manager()



#Usuário
class UsuarioMarcafut(models.Model):
        
    setor = models.ForeignKey(Setor)

    nome = models.CharField(
        max_length=255,
        null=False,
        blank=False        
    )

    usuario = models.CharField(
        max_length=255,
        null=False,
        blank=False        
    )
    
    senha = models.TextField(
        max_length=100,
        null=False,
        blank=False
    )
    
    
    def __unicode__(self):
        return self.nome

    objetos = models.Manager()

#Suprimento
class Suprimento(models.Model):

    usuario = models.ForeignKey(User, on_delete=models.CASCADE, default=4)
    
    nome = models.CharField(
        max_length=20,
        null=False,
        blank=False
    )
    partnumber = models.CharField(
        max_length=20,
        null=False,
        blank=False
    )
    serialnumber = models.CharField(
        max_length=20,
        null=False,
        blank=False
    )
    opcaosup_choices = (
        ("N", "-----"),
        ("E", "Entrada"),
        ("S", "Saída")
    )

    opcaosup = models.CharField(
        max_length=20,
        null=False,
        blank=False,
        choices=opcaosup_choices,
        default='N'

    )

    recol = models.CharField(
        max_length=20,
        null=True,
        blank=False
    )

    
    data = models.DateField(null=True, blank=True)

    def __unicode__(self):
        return self.nome
    
    objetos = models.Manager()

#Ferramenta
class Ferramenta(models.Model):

    usuario = models.ForeignKey(User, on_delete=models.CASCADE, default=4)
    
    nome = models.CharField(
        max_length=100,
        null=False,
        blank=False
    )
    descricao = models.CharField(
        max_length=1000,
        null=False,
        blank=False
    )
    
    def __unicode__(self):
        return self.nome
    
    objetos = models.Manager()

#Panes por MFL
class CodigoPane(models.Model):
    
    usuario = models.ForeignKey(User, on_delete=models.CASCADE, default=4)

    codigo = models.CharField(
        max_length=20,
        null=False,
        blank=False
    )
    descricao = models.TextField(
        max_length=1000,
        null=False,
        blank=False
    )
            
    def __unicode__(self):
        return self.descricao
    
    objetos = models.Manager()
