# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from manuti.models import RegistroDeManut, Equipamento, UsuarioMarcafut, Emergencia, Suprimento, Ferramenta, \
    CodigoPane
from django import forms

class DateInput(forms.DateInput):
    input_type = 'date'

class InsereRegistroForm(forms.ModelForm):
    
    class Meta:
        
        # Modelo base
        model = RegistroDeManut

        # Campos que estarão no form
        fields = ['data', 'tempo', 'anv', 'titulo', 'ocorrencia', 'solucao', 'resolvesiloms']
        widgets = {
            
            'data': DateInput()
        }

               
class InsereEquipamentoForm(forms.ModelForm):
    
   
    class Meta:
        # Modelo base
        model = Equipamento

        # Campos que estarão no form
        fields = [
            
            'nome',
            'partnumber',
            'serialnumber',
            'combs',
            'aeronave',
            'resolvecombs'
            
        ]

class InsereEmergenciaForm(forms.ModelForm):
    
    class Meta:
        
        # Modelo base
        model = Emergencia

        # Campos que estarão no form
        # Campos que estarão no form
        fields = [
            
            'data',
            'anv',
            'emergencia',
            'tipo',
            'ordemdeservico',
            'equipamento',
            'resolveemergencia'
            
            
        ]
        widgets = {
            
            'data': DateInput()
        }

class InsereUsuarioForm(forms.ModelForm):
    class Meta:
        model = UsuarioMarcafut

        fields = '__all__'

class InsereSuprimentoForm(forms.ModelForm):
    
    class Meta:
        
        # Modelo base
        model = Suprimento

        # Campos que estarão no form
        # Campos que estarão no form
        fields = [
            
            'nome',
            'partnumber',
            'serialnumber',
            'opcaosup',
            'recol',
            'data'
            
            
        ]

        widgets = {
            
            'data': DateInput()
            
        }

class InsereFerramentaForm(forms.ModelForm):
    class Meta:
        model = Ferramenta

        fields = [
            
            'nome',
            'descricao'
            
        ]

class InserePaneForm(forms.ModelForm):
    class Meta:
        model = CodigoPane

        fields = [
            
            'codigo',
            'descricao'
            
        ]


    