from views import IndexManutiTemplateView, RegistroCreateView, RegistroDeleteView, RegistroListView, \
    RegistroUpdateView, EquipamentoCreateView, EquipamentoDeleteView, EquipamentoListView, EquipamentoUpdateView, \
    AeronaveListView, EquipamentoAeronaveListView, EquipamentoAeronaveDeleteView, AeronaveRegistroListView, RegistroAeronaveListView, \
    AtividadeManutiTemplateView, CombsListView, SilomsListView, UsuarioCreateView, CombsResolvidoUpdateView, SilomsResolvidoUpdateView, \
    EmergenciaCreateView, EmergenciaListView, EmergenciaResolvidoUpdateView, EmergenciaUpdateView, SuprimentoCreateView, \
    SuprimentoDeleteView, SuprimentoListView, SuprimentoUpdateView, FerramentaCreateView, FerramentaDeleteView, FerramentaListView, \
    FerramentaUpdateView, PanesTemplateView
     

from django.conf.urls import url
from . import views


app_name = 'manuti'

urlpatterns = [
    # GET /
    
    #path('', IndexTemplateView.as_view(), name="index"),
    url(r'^$', IndexManutiTemplateView.as_view(), name="index"),
    
    
    url(r'^atividademanuti/', AtividadeManutiTemplateView.as_view(), name="atividade_manuti"),

    # GET /registro/cadastrar    
  
    url(r'^registro/cadastrar', RegistroCreateView.as_view(), name="cadastra_registro"),
    # GET /registros   
    url(r'^registros/', RegistroListView.as_view(), name="lista_registros"),
    # GET/POST /registro/{pk}    
    url(r'^registro/(?P<pk>[0-9]+)/$', RegistroUpdateView.as_view(), name="atualiza_registro"),
    # GET/POST /registros/excluir/{pk}
    url(r'^registro/excluir/(?P<pk>[0-9]+)/$', RegistroDeleteView.as_view(), name="deleta_registro"),

#-----------------------------------------------------------------------------------------------------
    
    # GET /equipamento/cadastrar    
    url(r'^equipamento/cadastrar', EquipamentoCreateView.as_view(), name="cadastra_equipamento"),
    # GET /equipamentos   
    url(r'^equipamentos/', EquipamentoListView.as_view(), name="lista_equipamentos"),
    # GET/POST /equipamento/{pk}    
    url(r'^equipamento/(?P<pk>[0-9]+)/$', EquipamentoUpdateView.as_view(), name="atualiza_equipamento"),
    # GET/POST /equipamentos/excluir/{pk}
    url(r'^equipamento/excluir/(?P<pk>[0-9]+)/$', EquipamentoDeleteView.as_view(), name="deleta_equipamento"),


#-----------------------------------------------------------------------------------------------------
    
# GET /usuario/cadastrar    
    url(r'^usuario/cadastrar', UsuarioCreateView.as_view(), name="cadastra_usuario"),

#-------------------------------------------------------------------------------------------------------------
# Equipamentos em Aeronave
    url(r'^aeronaves/', AeronaveListView.as_view(), name="lista_aeronaves"),  
    
    url(r'^visualizaanvs/', AeronaveRegistroListView.as_view(), name="lista_regaeronaves"),  
    
    url(r'^equipamentoanvs/(?P<fk>[0-9]+)/$', EquipamentoAeronaveListView.as_view(), name="lista_equipamentos_aeronave"),

    # GET/POST /equipamentosem Aeronave/excluir/{pk}
    url(r'^equipamentoanvsdelete/excluir/(?P<pk>[0-9]+)/$', EquipamentoAeronaveDeleteView.as_view(), name="deleta_equipamentoanv"),

    url(r'^registroanvs/(?P<fk>[0-9]+)/$', RegistroAeronaveListView.as_view(), name="lista_registros_aeronave"),
   
    # resolve siloms
    url(r'^silomsresol/(?P<pk>[0-9]+)/$', SilomsResolvidoUpdateView.as_view(), name="resolvesiloms"),
    url(r'^siloms', SilomsListView.as_view(), name="siloms"),
    
    # resolve combs
    url(r'^combsresol/(?P<pk>[0-9]+)/$', CombsResolvidoUpdateView.as_view(), name="resolvecombs"),
    url(r'^combs/', CombsListView.as_view(), name="combs"),
    
    # Emergencias
    # GET /emergencia/cadastrar    
    url(r'^emergencia/cadastrar', EmergenciaCreateView.as_view(), name="cadastra_emergencia"),

    # GET /lista de emergencias
    url(r'^emergenciasresol/(?P<pk>[0-9]+)/$', EmergenciaResolvidoUpdateView.as_view(), name="resolveemergencia"),  
    url(r'^emergencias/', EmergenciaListView.as_view(), name="emergencias"),

     # GET/POST /emergencia/{pk}    
    url(r'^emergencia/(?P<pk>[0-9]+)/$', EmergenciaUpdateView.as_view(), name="atualiza_emergencia"),

     # GET /suprimento/cadastrar    
    url(r'^suprimento/cadastrar', SuprimentoCreateView.as_view(), name="cadastra_suprimento"),
    # GET /suprimentos   
    url(r'^suprimentos/', SuprimentoListView.as_view(), name="lista_suprimento"),
    # GET/POST /suprimento/{pk}    
    url(r'^suprimento/(?P<pk>[0-9]+)/$', SuprimentoUpdateView.as_view(), name="atualiza_suprimento"),
    # GET/POST /suprimento/excluir/{pk}
    url(r'^suprimento/excluir/(?P<pk>[0-9]+)/$', SuprimentoDeleteView.as_view(), name="deleta_suprimento"),

     # GET /ferramenta/cadastrar    
    url(r'^ferramenta/cadastrar', FerramentaCreateView.as_view(), name="cadastra_ferramenta"),
    # GET /ferramenta   
    url(r'^ferramentas/', FerramentaListView.as_view(), name="ferramentas"),
    # GET/POST /ferramenta/{pk}    
    url(r'^ferramenta/(?P<pk>[0-9]+)/$', FerramentaUpdateView.as_view(), name="atualiza_ferramenta"),
    # GET/POST /ferramenta/excluir/{pk}
    url(r'^ferramenta/excluir/(?P<pk>[0-9]+)/$', FerramentaDeleteView.as_view(), name="deleta_ferramenta"),

    url(r'^panes/', PanesTemplateView.as_view(), name="panes"),

]